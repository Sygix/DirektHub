/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.utils;

import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.sygix.DirektHub.SQL.SQLGestion;

public class items {
	
	public static void cosmetiques(Player p){
		ItemStack item = new ItemStack(Material.ENDER_CHEST, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GOLD+""+ChatColor.BOLD+"Cosm�tiques");
	    item.setItemMeta(itemm);
	    p.getInventory().setItem(0, item);
	}
	
	public static void menu(Player p){
		ItemStack menu = new ItemStack(Material.WORKBENCH, 1);
	    ItemMeta menum = menu.getItemMeta();
	    menum.setDisplayName(ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu");
	    menu.setItemMeta(menum);
	    p.getInventory().setItem(4, menu);
	}
	
	public static void store(Player p){
		ItemStack store = new ItemStack(Material.DIAMOND, 1);
	    ItemMeta storem = store.getItemMeta();
	    storem.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Boutique");
	    store.setItemMeta(storem);
	    p.getInventory().setItem(8, store);
	}
	
	//Menu Principal
	
	public static ItemStack blackGlass(){
		ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte)15);
	    ItemMeta glassm = glass.getItemMeta();
	    glassm.setDisplayName(" ");
	    glass.setItemMeta(glassm);
		return glass;
	}
	
	public static ItemStack blueGlass(){
		ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte)11);
	    ItemMeta glassm = glass.getItemMeta();
	    glassm.setDisplayName(" ");
	    glass.setItemMeta(glassm);
		return glass;
	}
	
	public static ItemStack whiteGlass(){
		ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1);
	    ItemMeta glassm = glass.getItemMeta();
	    glassm.setDisplayName(" ");
	    glass.setItemMeta(glassm);
		return glass;
	}
	
	public static ItemStack switchHub(){
		ItemStack item = new ItemStack(Material.EYE_OF_ENDER, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Changer de hub");
	    itemm.addEnchant(Enchantment.MENDING, 1, false);
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack amis(){
		ItemStack item = new ItemStack(Material.MAP, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Amis");
	    itemm.addEnchant(Enchantment.MENDING, 1, false);
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack options(){
		ItemStack item = new ItemStack(Material.REDSTONE_COMPARATOR, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Options");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack tete(Player p){
		ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        meta.setOwner(p.getName());
        meta.setDisplayName(ChatColor.GREEN + p.getName());
        ArrayList<String> lore = new ArrayList<String>();
        try {
			lore.add(ChatColor.AQUA+"DirektCoins : "+ChatColor.YELLOW+""+SQLGestion.getCoins(p));
			lore.add(ChatColor.AQUA+"Temps : "+ChatColor.YELLOW+""+formatTime.formatminutes(SQLGestion.getTime(p)));
			lore.add(ChatColor.AQUA+"Halo : "+ChatColor.YELLOW+"x"+SQLGestion.getHalo(p));
			if(SQLGestion.getDateBuyHalo(p) != null){
				String date = SQLGestion.getDateBuyHalo(p);
				date = new StringBuilder(date).insert(4, "/").toString();
				date = new StringBuilder(date).insert(7, "/").toString();
				lore.add(ChatColor.AQUA+"Expiration Halo : "+ChatColor.YELLOW+""+date);
			}else{
				lore.add(ChatColor.AQUA+"Expiration Halo : "+ChatColor.YELLOW+"Jamais");
			}
		} catch (SQLException e) {
			lore.add(ChatColor.AQUA+"DirektCoins : "+ChatColor.RED+"Erreur");
			lore.add(ChatColor.AQUA+"Temps : "+ChatColor.RED+"Erreur");
			lore.add(ChatColor.AQUA+"Halo : "+ChatColor.RED+"Erreur");
			lore.add(ChatColor.AQUA+"Expiration Halo : "+ChatColor.RED+"Erreur");
			e.printStackTrace();
		}
        meta.setLore(lore);
        skull.setItemMeta(meta);
		return skull;
	}
	
	public static ItemStack spawn(){
		ItemStack item = new ItemStack(Material.DOUBLE_PLANT, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Spawn");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack jump(){
		ItemStack item = new ItemStack(Material.GOLD_BOOTS, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Jump");
	    itemm.addEnchant(Enchantment.MENDING, 1, false);
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack vote(){
		ItemStack item = new ItemStack(Material.PAPER, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote");
	    itemm.addEnchant(Enchantment.MENDING, 1, false);
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack ffa(){
		ItemStack item = new ItemStack(Material.DIAMOND_SWORD, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"FFA #SOON");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack tntrun(){
		ItemStack item = new ItemStack(Material.TNT, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"TnT"+ChatColor.GRAY+""+ChatColor.BOLD+"Run");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack murder(){
		ItemStack item = new ItemStack(Material.IRON_AXE, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Murder #SOON");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack corerush(){
		ItemStack item = new ItemStack(Material.CHORUS_FRUIT, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"NEW : "+ChatColor.RESET+""+ChatColor.DARK_PURPLE+""+ChatColor.BOLD+"CoreRush");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack labyrinthe(){
		ItemStack item = new ItemStack(Material.BED, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"SOON");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack arcade(){
		ItemStack item = new ItemStack(Material.ANVIL, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.DARK_RED+""+ChatColor.BOLD+"Arcade");
	    itemm.addEnchant(Enchantment.MENDING, 1, false);
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack retour(){
		ItemStack item = new ItemStack(Material.ARROW, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GREEN+""+ChatColor.BOLD+"Retour");
	    itemm.addEnchant(Enchantment.MENDING, 1, false);
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack twitter(){
		ItemStack item = new ItemStack(Material.BANNER, 1, (byte)6);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Twitter");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack youtube(){
		ItemStack item = new ItemStack(Material.BANNER, 1, (byte)1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"YouTube");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack halo1(){
		@SuppressWarnings("deprecation")
		ItemStack item = new ItemStack(351, 1, (byte)9);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Halo x1.2");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+"Dur�e : "+ChatColor.GREEN+"1 mois");
	    lore.add(ChatColor.AQUA+"Prix : "+ChatColor.GREEN+"1200 DirektCoins");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack halo2(){
		@SuppressWarnings("deprecation")
		ItemStack item = new ItemStack(351, 1, (byte)13);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Halo x1.3");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+"Dur�e : "+ChatColor.GREEN+"1 mois");
	    lore.add(ChatColor.AQUA+"Prix : "+ChatColor.GREEN+"1500 DirektCoins");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack halo3(){
		@SuppressWarnings("deprecation")
		ItemStack item = new ItemStack(351, 1, (byte)5);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Halo x1.4");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+"Dur�e : "+ChatColor.GREEN+"1 mois");
	    lore.add(ChatColor.AQUA+"Prix : "+ChatColor.GREEN+"1700 DirektCoins");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack serverStop() {
		ItemStack item = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta meta = item.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("Clique pour d�marrer le serveur !");
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack serverLobby() {
		ItemStack item = new ItemStack(Material.CHORUS_FRUIT);
		ItemMeta meta = item.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("Clique pour te connecter au serveur !");
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack serverGame() {
		ItemStack item = new ItemStack(Material.CHORUS_FRUIT_POPPED);
		ItemMeta meta = item.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("Une partie est d�j� en cours !");
		lore.add("Clique pour te rajouter � la file d'attente !");
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack serverBooting() {
		ItemStack item = new ItemStack(Material.REDSTONE_LAMP_ON);
		ItemMeta meta = item.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("Une partie est d�j� en cours !");
		lore.add("Clique pour te rajouter � la file d'attente !");
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack halobuyed(){
		@SuppressWarnings("deprecation")
		ItemStack item = new ItemStack(351, 1, (byte)1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Vous avez d�j� un halo");
	    item.setItemMeta(itemm);
		return item;
	}
	
	//Cosm�tiques Menu
		//Menu principal
	public static ItemStack gadget(){
		ItemStack item = new ItemStack(Material.BUCKET, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Gadgets");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack pets(){
		ItemStack item = new ItemStack(Material.EGG, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GREEN+""+ChatColor.BOLD+"Animaux de compagnie");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack montures(){
		ItemStack item = new ItemStack(Material.SADDLE, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Montures");
	    item.setItemMeta(itemm);
		return item;
	}
	
	public static ItemStack effets(){
		ItemStack item = new ItemStack(Material.EXP_BOTTLE, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.LIGHT_PURPLE+""+ChatColor.BOLD+"Effets");
	    item.setItemMeta(itemm);
		return item;
	}
		//gadget
	public static ItemStack elytra(){
		ItemStack elytra = new ItemStack(Material.ELYTRA, 1);
	    ItemMeta elytram = elytra.getItemMeta();
	    elytram.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Ailes");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Equipez des ailes qui vous permettrons de planner.");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux MiniVIP et +.");
	    elytram.setLore(lore);
	    elytram.addEnchant(Enchantment.DURABILITY, 255, false);
	    elytra.setItemMeta(elytram);
	    return elytra;
	}
	
	public static ItemStack bumper(){
		ItemStack item = new ItemStack(Material.BLAZE_ROD, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Bumper");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Faites un clic droit sur un joueur et envoyez le valser !");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
	    return item;
	}
	
	public static ItemStack teleporter(){
		ItemStack item = new ItemStack(Material.CHORUS_FRUIT, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Teleporter");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Faites un clic droit en l'air et t�l�porter les joueurs alentours");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
	    return item;
	}
		//Pets
	@SuppressWarnings("deprecation")
	public static ItemStack chien(){
		ItemStack item = new ItemStack(383, 1, (byte)95);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Chien");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Faites spawn votre chien qui vous suivra partout !");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux MiniVIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
	    return item;
	}
	@SuppressWarnings("deprecation")
	public static ItemStack poule(){
		ItemStack item = new ItemStack(383, 1, (byte)93);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Poule");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Faites spawn votre poulet qui vous suivra partout !");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
	    return item;
	}
		//Mountures
	public static ItemStack mule(){
		ItemStack item = new ItemStack(Material.CHEST, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Mule");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Faites spawn votre mule et explorez le HUB !");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux MiniVIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
	    return item;
	}
	public static ItemStack ane(){
		ItemStack item = new ItemStack(Material.WHEAT, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"�ne");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Faites spawn votre �ne et explorez le HUB !");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux MiniVIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
	    return item;
	}
	public static ItemStack cheval(){
		ItemStack item = new ItemStack(Material.SADDLE, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Cheval");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Faites spawn votre cheval et explorez le HUB !");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
	    return item;
	}
	public static ItemStack bonecheval(){
		ItemStack item = new ItemStack(Material.BONE, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Cheval-squelette");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Faites spawn votre cheval et explorez le HUB !");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
	    return item;
	}
	public static ItemStack rottencheval(){
		ItemStack item = new ItemStack(Material.ROTTEN_FLESH, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Cheval-zombie");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Faites spawn votre cheval et explorez le HUB !");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
	    return item;
	}
	public static ItemStack slime(){
		ItemStack item = new ItemStack(Material.SLIME_BALL, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.AQUA+""+ChatColor.BOLD+"Slime ~ SOON");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Faites spawn votre Slime et sauter partout !");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
	    return item;
	}
		//effets
	public static ItemStack kingflame(){
		ItemStack item = new ItemStack(Material.BLAZE_POWDER, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GOLD+"Roi des flammes");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Une couronne de flamme vous tourne autour de la t�te.");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux MiniVIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
		return item;
	}
	public static ItemStack feu(){
		ItemStack item = new ItemStack(Material.BLAZE_ROD, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GOLD+"Ceinture de feu");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Une ceinture de feu vous tourne autour.");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
		return item;
	}
	public static ItemStack music(){
		ItemStack item = new ItemStack(Material.NOTE_BLOCK, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GOLD+"Notes de musique");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Des notes de musique apparaissent au dessus de vous.");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
		return item;
	}
	@SuppressWarnings("deprecation")
	public static ItemStack nuage(){
		ItemStack item = new ItemStack(351, 1, (byte)15);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GOLD+"Nuage");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Un nuage vous suivra en permanance.");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
		return item;
	}
	public static ItemStack pluie(){
		ItemStack item = new ItemStack(Material.WATER_BUCKET, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GOLD+"Pluie");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Un nuage de pluie vous suivra en permanance.");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
		return item;
	}
	public static ItemStack helix(){
		ItemStack item = new ItemStack(Material.REDSTONE, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GOLD+"H�lix");
	    ArrayList<String> lore = new ArrayList<String>();
	    lore.add(ChatColor.AQUA+""+ChatColor.ITALIC+"Une h�lice de redstone vous tourne autour.");
	    lore.add(ChatColor.GREEN+""+ChatColor.ITALIC+"R�serv� aux VIP et +.");
	    itemm.setLore(lore);
	    item.setItemMeta(itemm);
		return item;
	}
}
