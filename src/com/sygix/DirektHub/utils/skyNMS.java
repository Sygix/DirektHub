/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.minecraft.server.v1_10_R1.EnumDifficulty;
import net.minecraft.server.v1_10_R1.EnumGamemode;
import net.minecraft.server.v1_10_R1.PacketPlayOutRespawn;
import net.minecraft.server.v1_10_R1.WorldType;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class skyNMS {
	
	//Premi�re m�thode, changement du ciel. Cr�er de la pluie non voulue.
	
	private static Class<?> getNMSClass(String nmsClassString) throws ClassNotFoundException
    {
        String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + ".";
        String name = "net.minecraft.server." + version + nmsClassString;
        Class<?> nmsClass = Class.forName(name);
        
        return nmsClass;
    }
    
	private static Object getConnection(Player player) throws SecurityException, NoSuchMethodException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        Method getHandle = player.getClass().getMethod("getHandle");
        Object nmsPlayer = getHandle.invoke(player);
        Field conField = nmsPlayer.getClass().getField("playerConnection");
        Object con = conField.get(nmsPlayer);
        return con;
    }
	
	public static void sendPacket(Player player, int number)
	{
		try
		{
			Class<?> packetClass = skyNMS.getNMSClass("PacketPlayOutGameStateChange");
	        Constructor<?> packetConstructor = packetClass.getConstructor(int.class, float.class);
	        Object packet = packetConstructor.newInstance(7, number);
	        Method sendPacket = getNMSClass("PlayerConnection").getMethod("sendPacket", skyNMS.getNMSClass("Packet"));
	        sendPacket.invoke(skyNMS.getConnection(player), packet);
		}
		catch (Exception e)
		{
			System.out.println("[skyNMS] Erreur: Le packet ne peut pas �tre envoy� ! [1]");
		}
	}
	
	//Seconde m�thode, changement de dimension. Chunks Bug�
	
	@SuppressWarnings("deprecation")
	public static void setDimension(Player player, int dimension){
		try{
			CraftPlayer cp = (CraftPlayer)player;
		    PacketPlayOutRespawn packet = new PacketPlayOutRespawn(dimension, EnumDifficulty.getById(player.getWorld().getDifficulty().getValue()), WorldType.NORMAL, EnumGamemode.getById(player.getGameMode().getValue()));
		    cp.getHandle().playerConnection.sendPacket(packet);
		    Chunk chunk = player.getWorld().getChunkAt(player.getLocation());
		    for (int x = -1; x < 1; x++) {
		      for (int z = -1; z < 1; z++) {
		    	  player.getWorld().refreshChunk(chunk.getX() + x, chunk.getZ() + z);
		      }
		    }
	    }
	    catch (Exception e)
		{
	    	e.printStackTrace();
			System.out.println("[skyNMS] Erreur: Le packet ne peut pas �tre envoy� ! [1]");
		}
	  }

}
