/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.utils;

public class formatTime {
	
	public static String formatminutes(long t){
		return t/30/24/60+ " mo "+ t/24/60%30 + " j " + t/60%24 + " h " + t%60 + " m";
	}

}
