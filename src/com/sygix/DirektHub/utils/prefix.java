/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.utils;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class prefix {
	
	static String cadmin = ChatColor.DARK_RED+"[Diable] ";
	static String cassist = ChatColor.DARK_RED+"[Démon] ";
	static String crespdev = ChatColor.RED+"[Démon] ";
	static String crespmodo = ChatColor.BLUE+"[Démon] ";
	static String cresparchi = ChatColor.DARK_PURPLE+"[Démon] ";
	static String cmoderateur = ChatColor.BLUE+"[Démon] ";
	static String cvip = ChatColor.GREEN+"[Vampire] ";
	static String cdeveloppeur = ChatColor.RED+"[Démon] ";
	static String cstaff = ChatColor.DARK_GREEN+"[Démon] ";
	static String carchitecte = ChatColor.DARK_PURPLE+"[Démon] ";
	static String chelper = ChatColor.DARK_AQUA+"[Démon] ";
	static String cvideaste = ChatColor.GOLD+"[Vidéaste] ";
	static String cfriend = ChatColor.DARK_PURPLE+"[DirektFriend] ";
	static String cdonateur = ChatColor.AQUA+"[Donateur] ";
	static String cminivip = ChatColor.DARK_GREEN+"[Zombie] ";
	static String cjoueur = ChatColor.GRAY+"[Citrouille] ";
	
	
	public static String getPrefix(Player p){
		String m;
		if(p.hasPermission("DirektServ.admin")){
			m = cadmin+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.assist")){
			m = cassist+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.respdev")){
			m = crespdev+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.respmodo")){
			m = crespmodo+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.resparchi")){
			m = cresparchi+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.developpeur")){
			m = cdeveloppeur+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.staff")){
			m = cstaff+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.moderateur")){
			m = cmoderateur+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.architecte")){
			m = carchitecte+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.helper")){
			m = chelper+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.videaste")){
			m = cvideaste+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.friend")){
			m = cfriend+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.donateur")){
			m = cdonateur+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.vip")){
			m = cvip+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else if(p.hasPermission("DirektServ.minivip")){
			m = cminivip+p.getDisplayName()+ChatColor.WHITE+" : ";
		}else{
			m = ChatColor.GRAY+p.getDisplayName()+" : ";
		}
		return m;
	}
	

}
