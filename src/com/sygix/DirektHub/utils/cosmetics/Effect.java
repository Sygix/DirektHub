/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.utils.cosmetics;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.sygix.DirektHub.Runnables.Cosmetics;

public class Effect {
	
	static String noperm = ChatColor.RED+"Vous n'avez pas le grade permettant l'utilisation de cet effet.";
	static String equiped = ChatColor.GOLD+"Vous venez d'�quiper l'effet : ";
	static String unequiped = ChatColor.GOLD+"Vous venez de retirer l'effet : ";
	static String unequipedlast = ChatColor.GOLD+"Vous venez de retirer l'effet pr�c�dent.";
	
	public static void setEffect(Player p, String perm, String effet, String effetname){
		if(p.hasPermission(perm)){
			if(Cosmetics.playercos.containsKey(p)){
				if(Cosmetics.playercos.get(p) == effet){
					Cosmetics.playercos.remove(p);
					p.sendMessage(unequiped+effetname);
				}else{
					p.sendMessage(unequipedlast);
					Cosmetics.playercos.replace(p, effet);
					p.sendMessage(equiped+effetname);
				}
			}else{
				Cosmetics.playercos.put(p, effet);
				p.sendMessage(equiped+effetname);
			}
		}else{
			p.sendMessage(noperm);
		}
	}

}
