/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.utils.cosmetics;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;

public class Pets {
	
static String noperm = ChatColor.RED+"Vous n'avez pas le grade pour avoir cet animal de compagnie.";
static String equiped = ChatColor.GOLD+"Vous venez de faire spawn votre animal de compagnie.";
static String unequiped = ChatColor.GOLD+"Vous venez de despawn votre animal de compagnie.";
public static HashMap<Player, Entity> pets = new HashMap<Player, Entity>();
	
	public static void setPets(Player p, String perm, EntityType pet){
		if(p.hasPermission(perm)){
			if(pets.containsKey(p)){
				if(pets.get(p).getType() == pet){
					Entity animal = pets.get(p);
					animal.remove();
					pets.remove(p, animal);
					p.sendMessage(unequiped);
				}else{
					Entity animal = pets.get(p);
					animal.remove();
					if(pet == EntityType.WOLF){
						Entity ent = wolf(p);
						pets.replace(p, ent);
					}
					if(pet == EntityType.CHICKEN){
						Entity ent = poule(p);
						pets.replace(p, ent);
					}
					p.sendMessage(equiped);
				}
			}else{
				if(pet == EntityType.WOLF){
					Entity ent = wolf(p);
					pets.replace(p, ent);
				}
				if(pet == EntityType.CHICKEN){
					Entity ent = poule(p);
					pets.replace(p, ent);
				}
				p.sendMessage(equiped);
			}
		}else{
			p.sendMessage(noperm);
		}
	}
	
	public static Wolf wolf(Player p){
		Wolf  wolf = (Wolf) p.getWorld().spawnEntity(p.getLocation(), EntityType.WOLF);
		wolf.setAdult();
		wolf.setTarget(p);
		wolf.setAngry(false);
		wolf.setTamed(true);
		wolf.setOwner(p);
		return wolf;
	}
	
	public static Chicken poule(Player p){
		Chicken  Chicken = (Chicken) p.getWorld().spawnEntity(p.getLocation(), EntityType.CHICKEN);
		Chicken.setAdult();
		Chicken.setTarget(p);
		return Chicken;
	}
	
	public static void removeAllPets(Player p){
		if(pets.containsKey(p)){
			Entity pet = pets.get(p);
			pet.remove();
		}
	}

}
