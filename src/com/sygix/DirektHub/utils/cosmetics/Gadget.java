/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.utils.cosmetics;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.sygix.DirektHub.utils.setRandom;

public class Gadget {
	
	public static HashMap<UUID, Long> hm = new HashMap<UUID, Long>();
	
	static String noperm = ChatColor.RED+"Vous n'avez pas le grade permettant l'utilisation de ce gadget.";
	static String equiped = ChatColor.GOLD+"Vous venez d'�quiper le gadget : ";
	static String unequiped = ChatColor.GOLD+"Vous venez de retirer le gadget : ";
	static String unequipedlast = ChatColor.GOLD+"Vous venez de retirer le gadget pr�c�dent.";
	
	public static void setGadget(Player p, ItemStack i, int slot, String perm){
		if(p.hasPermission(perm)){
			if(p.getInventory().getItem(slot) != null){
				if(p.getInventory().getItem(slot).getType() == i.getType()){
					p.sendMessage(unequiped+i.getItemMeta().getDisplayName());
					p.getInventory().clear(slot);
				}else{
					p.sendMessage(unequipedlast);
					p.sendMessage(equiped+i.getItemMeta().getDisplayName());
					p.getInventory().setItem(slot, i);
				}
			}else{
				p.sendMessage(equiped+i.getItemMeta().getDisplayName());
				p.getInventory().setItem(slot, i);
			}
		}else{
			p.sendMessage(noperm);
		}
	}
	
	public static void teleporter(Player p){
		if(hm.containsKey(p.getUniqueId())){
        	long time = hm.get(p.getUniqueId()) - System.currentTimeMillis();
        	if(!(time <= 0)){
        		long secondes = ((time / 1000) % 3600 ) % 60;
        		p.sendMessage(ChatColor.RED+"Vous devez attendre "+secondes+" secondes avant d'utiliser � nouveau ce gadget.");
            	return;
        	}
        }
		hm.put(p.getUniqueId(), (60 * 1000) + System.currentTimeMillis());
		for(Player pls : Bukkit.getOnlinePlayers()){
			double locx = pls.getLocation().getX();
			double locy = pls.getLocation().getY();
			double locz = pls.getLocation().getZ();
			double xmin = p.getLocation().getX() - 5;
			double ymin = p.getLocation().getY() - 5;
			double zmin = p.getLocation().getZ() - 5;
			double xmax = p.getLocation().getX() + 5;
			double ymax = p.getLocation().getY() + 5;
			double zmax = p.getLocation().getZ() + 5;
			Location loc = new Location(pls.getWorld(), locx+setRandom.getRandom(-10, 10), locy+setRandom.getRandom(0, 10), locz+setRandom.getRandom(-10, 10));
			if((locx > xmin) && (locy > ymin) && (locz > zmin)){
				if(loc.getBlock().isEmpty()){
					pls.teleport(loc);
				}
				if((locx < xmax) && (locy < ymax) && (locz < zmax)){
					if(loc.getBlock().isEmpty()){
						pls.teleport(loc);
					}
				}
			}
		}
	}

}
