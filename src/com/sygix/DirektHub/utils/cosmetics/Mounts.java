/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.utils.cosmetics;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.inventory.ItemStack;

public class Mounts {
	
	static String noperm = ChatColor.RED+"Vous n'avez pas le grade pour avoir cette monture.";
	
	public static void setHorseMount(Player p, Variant v, String perm){
		if(p.hasPermission(perm)){
			Horse  horseMount = (Horse) p.getWorld().spawnEntity(p.getLocation(), EntityType.HORSE);
			horseMount.setAdult();
			horseMount.setTamed(true);
			horseMount.getInventory().setSaddle(new ItemStack(Material.SADDLE, 1));
			horseMount.setVariant(v);
			horseMount.setCarryingChest(true);
			horseMount.setOwner(p);
			horseMount.setPassenger(p);
		}else{
			p.sendMessage(noperm);
		}
	}
	
	public static void setSlimeMount(Player p, String perm){
		if(p.hasPermission(perm)){
			Slime  SlimeMount = (Slime) p.getWorld().spawnEntity(p.getLocation(), EntityType.SLIME);
			SlimeMount.setAI(true);
			SlimeMount.setCanPickupItems(false);
			SlimeMount.setSilent(true);
			SlimeMount.setSize(3);
			SlimeMount.setPassenger(p);
		}else{
			p.sendMessage(noperm);
		}
	}

}
