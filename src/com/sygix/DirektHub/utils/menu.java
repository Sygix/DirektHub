/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.utils;


import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.sygix.DirektHub.SQL.SQLGestion;

public class menu {
	
	public static void OpenMenu(Player p){
		Inventory inv = Bukkit.createInventory(null , 54, ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu");
		int i;
		for(i = 9; i < 18; i++) {
			inv.setItem(i, items.blackGlass());
		}
		for(i = 19; i < 26; i++){
			inv.setItem(i, items.blueGlass());
		}
		for(i = 37; i < 39; i++){
			inv.setItem(i, items.blueGlass());
		}
		for(i = 42; i < 44; i++){
			inv.setItem(i, items.blueGlass());
		}
		for(i = 46; i < 49; i++){
			inv.setItem(i, items.blueGlass());
		}
		for(i = 50; i < 53; i++){
			inv.setItem(i, items.blueGlass());
		}
		inv.setItem(28, items.blueGlass());
		inv.setItem(34, items.blueGlass());
		inv.setItem(0, items.switchHub());
		inv.setItem(1, items.amis());
		inv.setItem(2, items.options());
		inv.setItem(4, items.tete(p));
		inv.setItem(6, items.spawn());
		inv.setItem(7, items.jump());
		inv.setItem(8, items.vote());
		inv.setItem(29, items.ffa());
		inv.setItem(31, items.tntrun());
		inv.setItem(33, items.murder());
		inv.setItem(39, items.corerush());
		inv.setItem(41, items.labyrinthe());
		inv.setItem(49, items.arcade());
		p.openInventory(inv);
	}
	
	public static void CoreRushMenu(Player p) {
		Inventory inv = Bukkit.createInventory(null, InventoryType.CHEST, ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu > CoreRush");
		int i;
		for(i = 0; i < 9; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		for(i = 9; i < 12; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		for(i = 15; i < 18; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		for(i = 18; i < 26; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		ItemStack coreRush1 = items.corerush();
		ItemMeta meta1 = coreRush1.getItemMeta();
		meta1.setDisplayName(ChatColor.DARK_PURPLE+""+ChatColor.BOLD+"CoreRush 1");
		coreRush1.setItemMeta(meta1);
		ItemStack coreRush2 = items.corerush();
		ItemMeta meta2 = coreRush2.getItemMeta();
		meta2.setDisplayName(ChatColor.DARK_PURPLE+""+ChatColor.BOLD+"CoreRush 2");
		coreRush2.setItemMeta(meta2);
		inv.setItem(12, coreRush1);
		inv.setItem(13, items.whiteGlass());
		inv.setItem(14, coreRush2);
		inv.setItem(26, items.retour());
		p.openInventory(inv);
	}
	
	public static void ArcadeMenu(Player p){
		Inventory inv = Bukkit.createInventory(null , InventoryType.CHEST, ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu > Arcade");
		int i;
		for(i = 0; i < 9; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		for(i = 9; i < 12; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		for(i = 15; i < 18; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		for(i = 18; i < 26; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		ItemStack Arcade1 = items.arcade();
		ItemMeta Arcade1m = items.arcade().getItemMeta();
		Arcade1m.setDisplayName(ChatColor.DARK_RED+""+ChatColor.BOLD+"Arcade 1");
		Arcade1.setItemMeta(Arcade1m);
		ItemStack Arcade2 = items.arcade();
		ItemMeta Arcade2m = items.arcade().getItemMeta();
		Arcade2m.setDisplayName(ChatColor.DARK_RED+""+ChatColor.BOLD+"Arcade 2");
		Arcade2.setItemMeta(Arcade2m);
		inv.setItem(12, Arcade1);
		inv.setItem(13, items.whiteGlass());
		inv.setItem(14, Arcade2);
		inv.setItem(26, items.retour());
		p.openInventory(inv);
	}
	
	public static void VoteMenu(Player p){
		Inventory inv = Bukkit.createInventory(null , InventoryType.CHEST, ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu > Vote");
		inv.setItem(0, items.twitter());
		inv.setItem(1, items.blueGlass());
		inv.setItem(2, items.blueGlass());
		inv.setItem(4, items.blueGlass());
		inv.setItem(6, items.blueGlass());
		inv.setItem(7, items.blueGlass());
		inv.setItem(8, items.youtube());
		inv.setItem(13, items.blueGlass());
		inv.setItem(22, items.blueGlass());
		inv.setItem(24, items.blueGlass());
		inv.setItem(25, items.blueGlass());
		ItemStack Vote1 = items.vote();
		ItemMeta Vote1m = items.vote().getItemMeta();
		Vote1m.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 1");
		Vote1.setItemMeta(Vote1m);
		ItemStack Vote2 = items.vote();
		ItemMeta Vote2m = items.vote().getItemMeta();
		Vote2m.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 2");
		Vote2.setItemMeta(Vote2m);
		ItemStack Vote3 = items.vote();
		ItemMeta Vote3m = items.vote().getItemMeta();
		Vote3m.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 3");
		Vote3.setItemMeta(Vote3m);
		ItemStack Vote4 = items.vote();
		ItemMeta Vote4m = items.vote().getItemMeta();
		Vote4m.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 4");
		Vote4.setItemMeta(Vote4m);
		ItemStack Vote5 = items.vote();
		ItemMeta Vote5m = items.vote().getItemMeta();
		Vote5m.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 5");
		Vote5.setItemMeta(Vote5m);
		ItemStack Vote6 = items.vote();
		ItemMeta Vote6m = items.vote().getItemMeta();
		Vote6m.setDisplayName(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 6");
		Vote6.setItemMeta(Vote6m);
		inv.setItem(3, Vote1);
		inv.setItem(5, Vote2);
		inv.setItem(12, Vote3);
		inv.setItem(14, Vote4);
		inv.setItem(21, Vote5);
		inv.setItem(23, Vote6);
		int i;
		for(i = 9; i < 12; i++) {
			inv.setItem(i, items.blueGlass());
		}
		for(i = 15; i < 18; i++) {
			inv.setItem(i, items.blueGlass());
		}
		for(i = 18; i < 21; i++) {
			inv.setItem(i, items.blueGlass());
		}
		inv.setItem(26, items.retour());
		p.openInventory(inv);
	}
	
	public static void PlayerMenu(Player p){
		Inventory inv = Bukkit.createInventory(null , InventoryType.CHEST, ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu > "+p.getName());
		int i;
		for(i = 0; i < 9; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		for(i = 9; i < 11; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		for(i = 16; i < 18; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		for(i = 18; i < 26; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		inv.setItem(12, items.whiteGlass());
		inv.setItem(14, items.whiteGlass());
		inv.setItem(26, items.retour());
		try {
			if((SQLGestion.getDateBuyHalo(p) != null) || SQLGestion.getHalo(p) >= 1.5){
				inv.setItem(11, items.halobuyed());
				inv.setItem(13, items.halobuyed());
				inv.setItem(15, items.halobuyed());
			}else{
				inv.setItem(11, items.halo1());
				inv.setItem(13, items.halo2());
				inv.setItem(15, items.halo3());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		p.openInventory(inv);
	}
	
	public static void CosmetiquesMenu(Player p){
		Inventory inv = Bukkit.createInventory(null , InventoryType.CHEST, ChatColor.GOLD+""+ChatColor.BOLD+"Cosmétiques");
		int i;
		for(i = 0; i < 10; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		for(i = 17; i < 26; i++) {
			inv.setItem(i, items.whiteGlass());
		}
		inv.setItem(11, items.whiteGlass());
		inv.setItem(13, items.whiteGlass());
		inv.setItem(15, items.whiteGlass());
		inv.setItem(10, items.gadget());
		inv.setItem(12, items.pets());
		inv.setItem(14, items.montures());
		inv.setItem(16, items.effets());
		inv.setItem(26, items.retour());
		p.openInventory(inv);
	}
	
	public static void CosmetiquesGadgets(Player p){
		Inventory inv = Bukkit.createInventory(null , 54, ChatColor.GOLD+""+ChatColor.BOLD+"Cosmétiques > Gadgets");
		inv.setItem(0, items.elytra());
		inv.setItem(1, items.bumper());
		inv.setItem(2, items.teleporter());
		inv.setItem(53, items.retour());
		p.openInventory(inv);
	}
	
	public static void CosmetiquesPets(Player p){
		Inventory inv = Bukkit.createInventory(null , 54, ChatColor.GOLD+""+ChatColor.BOLD+"Cosmétiques > Pets");
		/*inv.setItem(0, items.chien());
		inv.setItem(1, items.poule());*/
		inv.setItem(53, items.retour());
		p.openInventory(inv);
	}
	
	public static void CosmetiquesMounts(Player p){
		Inventory inv = Bukkit.createInventory(null , 54, ChatColor.GOLD+""+ChatColor.BOLD+"Cosmétiques > Montures");
		inv.setItem(0, items.mule());
		inv.setItem(1, items.ane());
		inv.setItem(2, items.cheval());
		inv.setItem(3, items.bonecheval());
		inv.setItem(4, items.rottencheval());
		//inv.setItem(5, items.slime());
		inv.setItem(53, items.retour());
		p.openInventory(inv);
	}
	
	public static void CosmetiquesEffets(Player p){
		Inventory inv = Bukkit.createInventory(null , 54, ChatColor.GOLD+""+ChatColor.BOLD+"Cosmétiques > Effets");
		inv.setItem(0, items.kingflame());
		inv.setItem(1, items.feu());
		inv.setItem(2, items.music());
		inv.setItem(3, items.nuage());
		inv.setItem(4, items.pluie());
		inv.setItem(5, items.helix());
		inv.setItem(53, items.retour());
		p.openInventory(inv);
	}

}
