/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.Events;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

public class EventManager {
	
	public static void registerEvent(Plugin pl){
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new PlayerJoin(), pl);
		pm.registerEvents(new PlayerQuit(), pl);
		pm.registerEvents(new PlayerEvents(), pl);
		pm.registerEvents(new NaturalEvents(), pl);
		pm.registerEvents(new CosmeticsEvent(), pl);
	}

}
