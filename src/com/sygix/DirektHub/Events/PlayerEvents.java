/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.Events;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.util.Vector;

import com.sygix.DirektHub.DirektHub;
import com.sygix.DirektHub.SQL.SQLGestion;
import com.sygix.DirektHub.utils.menu;
import com.sygix.DirektHub.utils.prefix;

@SuppressWarnings("deprecation")
public class PlayerEvents implements Listener {
	
	@EventHandler
	public void onEntityDamageEvent (EntityDamageEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerMoveEvent (PlayerMoveEvent e){
		Player p = e.getPlayer();
		World w = p.getWorld();
		Location loc = p.getLocation();
		Block b = p.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ());
		double locx = loc.getX();
		double locy = loc.getY();
		double locz = loc.getZ();
		int xmin = -1118;
		int ymin = 0;
		int zmin = 323;
		int xmax = -810;
		int ymax = 256;
		int zmax = 699;
		if((locx > xmin) && (locy > ymin) && (locz > zmin)){
			if((locx < xmax) && (locy < ymax) && (locz < zmax)){
			}else{
				p.sendTitle(ChatColor.DARK_RED+"Mais o� vas-tu comme �a ?", "");
				p.teleport(w.getSpawnLocation());
				p.playSound(loc, Sound.ENTITY_GHAST_SCREAM, 2F, 2F);
			}
		}else{
			p.sendTitle(ChatColor.DARK_RED+"Mais o� vas-tu comme �a ?", "");
			p.teleport(w.getSpawnLocation());
			p.playSound(loc, Sound.ENTITY_GHAST_SCREAM, 2F, 2F);
		}
		if ((b != null) && (b.getType().equals(Material.EMERALD_BLOCK))){
			p.setVelocity(new Vector(loc.getDirection().getX(), loc.getDirection().getY()+1.1, loc.getDirection().getZ()).multiply(2));
		}
		//Jump.execute(p, b, loc, w);
	}
	
	@EventHandler
	public void onPlayerDrop(PlayerDropItemEvent e){
		Player p = e.getPlayer();
		if(!p.hasPermission("DirektHub.admin")){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void Inventoryclick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		if(e.getClickedInventory() == null){
			return;
		}
		if(e.getClickedInventory().getName().equals(ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu")){
			if(e.getCurrentItem().getType().equals(Material.DOUBLE_PLANT)){
				p.teleport(p.getLocation().getWorld().getSpawnLocation());
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.GOLD_BOOTS)){
				p.teleport(new Location(p.getLocation().getWorld(), -947.5, 77, 521.5));
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.TNT)){
				DirektHub.getInstance().SwitchServer(p, "tntrun");
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.ANVIL)){
				menu.ArcadeMenu(p);
			}
			if(e.getCurrentItem().getType().equals(Material.CHORUS_FRUIT)) {
				menu.CoreRushMenu(p);
			}
			if(e.getCurrentItem().getType().equals(Material.MAP)){
				TextComponent list = new TextComponent("- Liste de mes amis");
				list.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
				list.setBold(true);
				list.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend list"));
				TextComponent msg = new TextComponent("- Envoyer un message");
				msg.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
				msg.setBold(true);
				msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(net.md_5.bungee.api.ChatColor.AQUA+"/msg <pseudo> <message>").create()));
				msg.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/msg "));
				TextComponent add = new TextComponent("- Ajouter un ami");
				add.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
				add.setBold(true);
				add.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(net.md_5.bungee.api.ChatColor.AQUA+"/friend add <pseudo>").create()));
				add.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/friend add "));
				TextComponent del = new TextComponent("- Retirer un ami");
				del.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
				del.setBold(true);
				del.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(net.md_5.bungee.api.ChatColor.AQUA+"/friend remove <pseudo>").create()));
				del.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/friend remove "));
				TextComponent jump = new TextComponent("- Rejoindre un ami");
				jump.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
				jump.setBold(true);
				jump.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(net.md_5.bungee.api.ChatColor.AQUA+"/friend jump <pseudo>").create()));
				jump.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/friend jump "));
				p.sendMessage(ChatColor.AQUA+""+ChatColor.BOLD+"Que souhaitez-vous faire ?"+ChatColor.RESET+""+ChatColor.AQUA+""+ChatColor.ITALIC+" (Cliquez sur le message !)");
				p.spigot().sendMessage(list);
				p.spigot().sendMessage(msg);
				p.spigot().sendMessage(add);
				p.spigot().sendMessage(del);
				p.spigot().sendMessage(jump);
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.PAPER)){
				menu.VoteMenu(p);
			}
			if(e.getCurrentItem().getType().equals(Material.SKULL_ITEM)){
				menu.PlayerMenu(p);
			}
			e.setCancelled(true);
			return;
		}
		if(e.getClickedInventory().getName().equalsIgnoreCase(ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu > CoreRush")) {
			if(e.getCurrentItem().getType().equals(Material.ARROW)){
				menu.OpenMenu(p);
			}
			
		}
		if(e.getClickedInventory().getName().equals(ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu > Arcade")){
			if(e.getCurrentItem().getType().equals(Material.ARROW)){
				menu.OpenMenu(p);
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.DARK_RED+""+ChatColor.BOLD+"Arcade 1")){
				DirektHub.getInstance().SwitchServer(p, "arcade1");
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.DARK_RED+""+ChatColor.BOLD+"Arcade 2")){
				DirektHub.getInstance().SwitchServer(p, "arcade2");
				p.closeInventory();
			}
			e.setCancelled(true);
			return;
		}
		if(e.getClickedInventory().getName().equals(ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu > Vote")){
			if(e.getCurrentItem().getType().equals(Material.ARROW)){
				menu.OpenMenu(p);
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 1")){
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 2")){
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 3")){
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 4")){
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 5")){
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA+""+ChatColor.ITALIC+"Vote 6")){
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA+""+ChatColor.BOLD+"Twitter")){
				TextComponent tc = new TextComponent("Notre Twitter");
				tc.setColor(net.md_5.bungee.api.ChatColor.AQUA);
				tc.setBold(true);
				tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(net.md_5.bungee.api.ChatColor.YELLOW+"https://twitter.com/DirektServ").create()));
				tc.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://twitter.com/DirektServ"));
				p.spigot().sendMessage(tc);
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.RED+""+ChatColor.BOLD+"YouTube")){
				TextComponent tc = new TextComponent("Notre YoutTube");
				tc.setColor(net.md_5.bungee.api.ChatColor.RED);
				tc.setBold(true);
				tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(net.md_5.bungee.api.ChatColor.YELLOW+"http://sygi.tk/2cfqgEb").create()));
				tc.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://sygi.tk/2cfqgEb"));
				p.spigot().sendMessage(tc);
				p.closeInventory();
			}
			e.setCancelled(true);
			return;
		}
		if(e.getClickedInventory().getName().equals(ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu > "+p.getName())){
			if(e.getCurrentItem().getType().equals(Material.ARROW)){
				menu.OpenMenu(p);
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.RED+""+ChatColor.BOLD+"Halo x1.2")){
				try {
					if(SQLGestion.getCoins(p) >= 1200){
						DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.MONTH, 1);
						SQLGestion.setCoins(p, SQLGestion.getCoins(p) - 1200);
						SQLGestion.setHalo(p, 1.2);
						SQLGestion.setDateBuyHalo(p, dateFormat.format(cal.getTime()));
						p.closeInventory();
						p.sendMessage(ChatColor.AQUA+"Vous venez d'acheter un Halo x1.2 pour 1200 DirektCoins !");
					}else{
						p.closeInventory();
						p.sendMessage(ChatColor.RED+"Vous n'avez pas assez de DirektCoins !");
					}
				} catch (SQLException e1) {
					p.sendMessage("Une Erreur[Halo1.2], c'est produite !");
					e1.printStackTrace();
				}
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.RED+""+ChatColor.BOLD+"Halo x1.3")){
				try {
					if(SQLGestion.getCoins(p) >= 1500){
						DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.MONTH, 1);
						SQLGestion.setCoins(p, SQLGestion.getCoins(p) - 1500);
						SQLGestion.setHalo(p, 1.3);
						SQLGestion.setDateBuyHalo(p, dateFormat.format(cal.getTime()));
						p.closeInventory();
						p.sendMessage(ChatColor.AQUA+"Vous venez d'acheter un Halo x1.3 pour 1500 DirektCoins !");
					}else{
						p.closeInventory();
						p.sendMessage(ChatColor.RED+"Vous n'avez pas assez de DirektCoins !");
					}
				} catch (SQLException e1) {
					p.sendMessage("Une Erreur[Halo1.3], c'est produite !");
					e1.printStackTrace();
				}
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.RED+""+ChatColor.BOLD+"Halo x1.4")){
				try {
					if(SQLGestion.getCoins(p) >= 1700){
						DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.MONTH, 1);
						SQLGestion.setCoins(p, SQLGestion.getCoins(p) - 1700);
						SQLGestion.setHalo(p, 1.4);
						SQLGestion.setDateBuyHalo(p, dateFormat.format(cal.getTime()));
						p.closeInventory();
						p.sendMessage(ChatColor.AQUA+"Vous venez d'acheter un Halo x1.4 pour 1700 DirektCoins !");
					}else{
						p.closeInventory();
						p.sendMessage(ChatColor.RED+"Vous n'avez pas assez de DirektCoins !");
					}
				} catch (SQLException e1) {
					p.sendMessage("Une Erreur[Halo1.4], c'est produite !");
					e1.printStackTrace();
				}
			}
			e.setCancelled(true);
		}
		if(!p.hasPermission("DirektHub.admin")){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		Player p = e.getPlayer();
		if(!p.hasPermission("DirektHub.admin")){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent e){
		Entity et = e.getRightClicked();
		if(et.getType() != EntityType.PLAYER){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		Action a = e.getAction();
		if((a == Action.RIGHT_CLICK_AIR) || (a == Action.RIGHT_CLICK_BLOCK) || (a == Action.LEFT_CLICK_BLOCK) || (a == Action.LEFT_CLICK_AIR)){
			//p.sendMessage("18");
			if(p.getItemInHand() == null){return;}
			if(p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.GOLD+""+ChatColor.BOLD+"Cosm�tiques")){
				menu.CosmetiquesMenu(p);
				e.setCancelled(true);
				return;
			}
			if(p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.DARK_RED+""+ChatColor.BOLD+"Menu")){
				menu.OpenMenu(p);
				e.setCancelled(true);
				return;
			}
			if(p.getItemInHand().getType() == Material.DIAMOND){
				//p.sendMessage("12");
				p.chat("/shop");
				e.setCancelled(true);
				return;
			}else{
				return;
			}
		}
		if(!p.hasPermission("DirektHub.admin")){
			if(e.getAction().equals(Action.PHYSICAL)){
			    e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerPickupItem(PlayerPickupItemEvent e){
		Player p = e.getPlayer();
		if(!p.hasPermission("DirektHub.admin")){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent e){
		Player p = e.getPlayer();
		if(!p.hasPermission("DirektHub.admin")){
			if(!e.getMessage().equalsIgnoreCase("/store")){
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerChat(PlayerChatEvent e){
		Player p = e.getPlayer();
		String m = e.getMessage();
		if((m.contains("�") || m.contains("&")) && (p.hasPermission("DirektServ.helper") || p.hasPermission("DirektServ.architecte") || p.hasPermission("DirektServ.developpeur"))){
			m = ChatColor.translateAlternateColorCodes('�', m);
			m = ChatColor.translateAlternateColorCodes('&', m);
		}
		e.setFormat(prefix.getPrefix(p)+m);
		p.getWorld().strikeLightning(p.getLocation());
	}
	
	@EventHandler
	public void onPlayerPortal(PlayerPortalEvent e){
		e.setCancelled(true);
	}

}
