/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.Events;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.sygix.DirektHub.utils.items;
import com.sygix.DirektHub.utils.scoreboard;

public class PlayerJoin implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		p.teleport(p.getLocation().getWorld().getSpawnLocation());
		p.getInventory().clear();
		p.sendTitle(ChatColor.GOLD+"Joyeux "+ChatColor.DARK_PURPLE+"HALLOWEEN "+ChatColor.GOLD+"sur "+ChatColor.DARK_GRAY+"Direkt"+ChatColor.RED+"Serv", "");
		if(p.hasPermission("DirektHub.fly")){
			p.setAllowFlight(true);
			p.setFlying(true);
		}
		items.store(p);
		items.menu(p);
		items.cosmetiques(p);
		scoreboard.team(p);
		if(p.hasPermission("DirektServ.admin")){
			e.setJoinMessage(ChatColor.DARK_RED+"[Diable] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.assist")){
			e.setJoinMessage(ChatColor.DARK_RED+"[D�mon] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.respdev")){
			e.setJoinMessage(ChatColor.RED+"[D�mon] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.respmodo")){
			e.setJoinMessage(ChatColor.BLUE+"[D�mon] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.resparchi")){
			e.setJoinMessage(ChatColor.DARK_PURPLE+"[D�mon] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.developpeur")){
			e.setJoinMessage(ChatColor.RED+"[D�mon] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.staff")){
			e.setJoinMessage(ChatColor.DARK_GREEN+"[D�mon] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.moderateur")){
			e.setJoinMessage(ChatColor.BLUE+"[D�mon] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.architecte")){
			e.setJoinMessage(ChatColor.DARK_PURPLE+"[D�mon] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.helper")){
			e.setJoinMessage(ChatColor.DARK_AQUA+"[D�mon] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.videaste")){
			e.setJoinMessage(ChatColor.GOLD+"[Vid�aste] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.friend")){
			e.setJoinMessage(ChatColor.DARK_PURPLE+"[DirektFriend] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.donateur")){
			e.setJoinMessage(ChatColor.AQUA+"[Donateur] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.vip")){
			e.setJoinMessage(ChatColor.GREEN+"[Vampire] "+p.getDisplayName()+" a rejoint le hub !");
		}else if(p.hasPermission("DirektServ.minivip")){
			e.setJoinMessage(ChatColor.DARK_GREEN+"[Zombie] "+p.getDisplayName()+" a rejoint le hub !");
		}else{
			e.setJoinMessage("");
		}
	}

}
