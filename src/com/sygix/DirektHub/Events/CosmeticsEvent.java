/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.Events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.inventory.HorseInventory;
import org.bukkit.util.Vector;

import com.sygix.DirektHub.utils.items;
import com.sygix.DirektHub.utils.menu;
import com.sygix.DirektHub.utils.cosmetics.Effect;
import com.sygix.DirektHub.utils.cosmetics.Gadget;
import com.sygix.DirektHub.utils.cosmetics.Mounts;
import com.sygix.DirektHub.utils.cosmetics.ParticleEffect;
import com.sygix.DirektHub.utils.cosmetics.Pets;

@SuppressWarnings("deprecation")
public class CosmeticsEvent implements Listener{
	
	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent e){
		Entity et = e.getRightClicked();
		Player p = e.getPlayer();
		if(!(et instanceof Player)){
			e.setCancelled(true);
		}else if(Bukkit.getServer().getOnlinePlayers().contains(et)){
			if(p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.AQUA+""+ChatColor.BOLD+"Bumper") && p.getItemInHand() != null){
				Location loc = p.getLocation();
				et.setVelocity(new Vector(loc.getDirection().getX(), et.getLocation().getDirection().getY()+1, loc.getDirection().getZ()).multiply(1));
				ParticleEffect.SMOKE_NORMAL.display(0, 0, 0, 1, 100, et.getLocation().add(0, 2, 0), 10);
			}
		}
	}
	
	@EventHandler
	public void vehicleExit(VehicleExitEvent e){ // Disparition de l'entit� lorsque l'on quitte le vehicule
		Vehicle v = e.getVehicle();
		v.remove();
	}
	
	@EventHandler
	public void playerInventory(InventoryOpenEvent e) {
		if(e.getInventory() instanceof HorseInventory) { //Emp�cher l�interaction avec l'inventaire du cheval
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void EntityInteract(EntityInteractEvent e){ //Emp�cher l'interaction par les entit�s
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		Action a = e.getAction();
		if(a == Action.RIGHT_CLICK_AIR){
			if((p.getItemInHand().getType().equals(Material.CHORUS_FRUIT) && (p.getItemInHand()!= null))){
				Gadget.teleporter(p);
				e.setCancelled(true);
				return;
			}else{
				return;
			}
		}
		if(!p.hasPermission("DirektHub.admin")){
			if(e.getAction().equals(Action.PHYSICAL)){
			    e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void PlayerTeleport(PlayerTeleportEvent e){
		Player p = e.getPlayer();
		if(Pets.pets.containsKey(p)){
			Entity pet = Pets.pets.get(p);
			Location loc = p.getLocation();
			pet.teleport(loc);
		}
	}
	
	@EventHandler
	public void Inventoryclick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		if(e.getClickedInventory() == null){
			return;
		}
		if(e.getClickedInventory().getName().equals(ChatColor.GOLD+""+ChatColor.BOLD+"Cosm�tiques")){
			if(e.getCurrentItem().getType().equals(Material.BUCKET)){
				menu.CosmetiquesGadgets(p);
			}
			if(e.getCurrentItem().getType().equals(Material.EGG)){
				menu.CosmetiquesPets(p);
			}
			if(e.getCurrentItem().getType().equals(Material.SADDLE)){
				menu.CosmetiquesMounts(p);
			}
			if(e.getCurrentItem().getType().equals(Material.EXP_BOTTLE)){
				menu.CosmetiquesEffets(p);
			}
			if(e.getCurrentItem().getType().equals(Material.ARROW)){
				p.closeInventory();
			}
			e.setCancelled(true);
			return;
		}
		if(e.getClickedInventory().getName().equals(ChatColor.GOLD+""+ChatColor.BOLD+"Cosm�tiques > Gadgets")){
			if(e.getCurrentItem().getType().equals(Material.ELYTRA)){
				Gadget.setGadget(p, items.elytra(), 38, "DirektServ.minivip");
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.BLAZE_ROD)){
				Gadget.setGadget(p, items.bumper(), 1, "DirektServ.vip");
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.CHORUS_FRUIT)){
				Gadget.setGadget(p, items.teleporter(), 1, "DirektServ.vip");
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.ARROW)){
				menu.CosmetiquesMenu(p);
			}
			e.setCancelled(true);
			return;
		}
		if(e.getClickedInventory().getName().equals(ChatColor.GOLD+""+ChatColor.BOLD+"Cosm�tiques > Pets")){
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA+""+ChatColor.BOLD+"Chien")){
				Pets.setPets(p, "DirektServ.minivip", EntityType.WOLF);
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA+""+ChatColor.BOLD+"Poule")){
				Pets.setPets(p, "DirektServ.VIP", EntityType.CHICKEN);
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.ARROW)){
				menu.CosmetiquesMenu(p);
			}
			e.setCancelled(true);
			return;
		}
		if(e.getClickedInventory().getName().equals(ChatColor.GOLD+""+ChatColor.BOLD+"Cosm�tiques > Montures")){
			if(e.getCurrentItem().getType().equals(Material.CHEST)){
				Mounts.setHorseMount(p, Variant.MULE, "DirektServ.minivip");
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.WHEAT)){
				Mounts.setHorseMount(p, Variant.DONKEY, "DirektServ.minivip");
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.SADDLE)){
				Mounts.setHorseMount(p, Variant.HORSE, "DirektServ.vip");
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.BONE)){
				Mounts.setHorseMount(p, Variant.SKELETON_HORSE, "DirektServ.vip");
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.ROTTEN_FLESH)){
				Mounts.setHorseMount(p, Variant.UNDEAD_HORSE, "DirektServ.vip");
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.SLIME_BALL)){
				//Mounts.setSlimeMount(p, "DirektServ.vip");
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.ARROW)){
				menu.CosmetiquesMenu(p);
			}
			e.setCancelled(true);
			return;
		}
		if(e.getClickedInventory().getName().equals(ChatColor.GOLD+""+ChatColor.BOLD+"Cosm�tiques > Effets")){
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD+"Roi des flammes")){
				Effect.setEffect(p, "DirektServ.minivip", "kingflame", ChatColor.AQUA+"Roi des flammes");
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD+"Ceinture de feu")){
				Effect.setEffect(p, "DirektServ.vip", "feu", ChatColor.AQUA+"Ceinture de feu");
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD+"Notes de musique")){
				Effect.setEffect(p, "DirektServ.vip", "music", ChatColor.AQUA+"Notes de musique");
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD+"Nuage")){
				Effect.setEffect(p, "DirektServ.vip", "nuage", ChatColor.AQUA+"Nuage");
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD+"Pluie")){
				Effect.setEffect(p, "DirektServ.vip", "pluie", ChatColor.AQUA+"Pluie");
				p.closeInventory();
			}
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD+"H�lix")){
				Effect.setEffect(p, "DirektServ.vip", "helix", ChatColor.AQUA+"H�lix");
				p.closeInventory();
			}
			if(e.getCurrentItem().getType().equals(Material.ARROW)){
				menu.CosmetiquesMenu(p);
			}
			e.setCancelled(true);
			return;
		}
	}

}
