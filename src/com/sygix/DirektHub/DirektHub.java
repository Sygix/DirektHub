/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub;

import java.io.File;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.sygix.DirektHub.Events.EventManager;
import com.sygix.DirektHub.Runnables.Cosmetics;
import com.sygix.DirektHub.SQL.SQLGestion;
import com.sygix.DirektHub.utils.scoreboard;
import com.sygix.DirektHub.utils.cosmetics.Pets;

public class DirektHub extends JavaPlugin{
	
public static DirektHub instance;
	
	public static DirektHub getInstance() {
		return instance;
	}
	
	public void onEnable(){
		getLogger().info("Chargement ...");
		instance = this;
		Cosmetics.start();
		scoreboard.setteam();
		EventManager.registerEvent(this);
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		try {
			SQLGestion.connect();
			SQLGestion.registerPlugin();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		getLogger().info("Chargement termine !");
	}
	
	public void onDisable(){
		getLogger().info("Dechargement ...");
		for(Player pls : Bukkit.getServer().getOnlinePlayers()){
			Pets.removeAllPets(pls);
		}
		for(World w : Bukkit.getWorlds()){
			File folder = new File(w.getWorldFolder(), "playerdata");
			for(File f : folder.listFiles()){
				f.delete();
			}
		}
		try {
			SQLGestion.disconnect();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		getLogger().info("Dechargement termine !");
	}
	
	public void SwitchServer(Player p, String s){
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
	    out.writeUTF("Connect");
	    out.writeUTF(s);
	    p.sendPluginMessage(this, "BungeeCord", out.toByteArray());
	}

}
