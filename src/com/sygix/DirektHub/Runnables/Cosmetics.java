/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.Runnables;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.sygix.DirektHub.DirektHub;
import com.sygix.DirektHub.utils.cosmetics.ParticleEffect;

public class Cosmetics {
	
	public static int task;
	public static boolean run = false;
	
	public static HashMap<Player, String> playercos = new HashMap<Player, String>();
	
	@SuppressWarnings("deprecation")
	public static void start(){
		
		if(run == true){
			return;
		}
		run = true;
		task = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(DirektHub.getInstance(), new BukkitRunnable() {
			
			int count = 0;
			double feuheight = 0;
			double helixheight = 0;

			@Override
			public void run() {
				for(Player pls : Bukkit.getServer().getOnlinePlayers()){
					if(!playercos.containsKey(pls)){
					}else{
						double valeure = (count * Math.PI)/6;
						if(playercos.get(pls) == "kingflame"){
							Location loc = new Location(pls.getWorld(), pls.getLocation().getX() + Math.cos(valeure)/2.5, pls.getLocation().getY() + 2,  pls.getLocation().getZ() + Math.sin(valeure)/2.5);
							ParticleEffect.FLAME.display(0f, 0f, 0f, 0f, 1, loc, 15);
						}
						if(playercos.get(pls) == "feu"){
							Location loc = new Location(pls.getWorld(), pls.getLocation().getX() + Math.cos(valeure)/2.5, pls.getLocation().getY() + feuheight,  pls.getLocation().getZ() + Math.sin(valeure)/2.5);
							ParticleEffect.FLAME.display(0f, 0f, 0f, 0f, 1, loc, 15);
						}
						if(playercos.get(pls) == "music"){
							if(count == 1 || count == 4 || count == 7 || count == 10 || count == 13 || count == 16 || count == 19 || count == 22){
								Location loc = new Location(pls.getWorld(), pls.getLocation().getX(), pls.getLocation().getY()+2.5,  pls.getLocation().getZ());
								ParticleEffect.NOTE.display(0f, 0f, 0f, 0f, 1, loc, 15);
							}
						}
						if(playercos.get(pls) == "nuage"){
							Location loc = new Location(pls.getWorld(), pls.getLocation().getX(), pls.getLocation().getY()+2.5,  pls.getLocation().getZ());
							ParticleEffect.CLOUD.display(0.3f, 0f, 0.3f, 0f, 3, loc, 15);
						}
						if(playercos.get(pls) == "pluie"){
							Location loc = new Location(pls.getWorld(), pls.getLocation().getX(), pls.getLocation().getY()+2.5,  pls.getLocation().getZ());
							Location loc2 = new Location(pls.getWorld(), pls.getLocation().getX(), pls.getLocation().getY()+2.2,  pls.getLocation().getZ());
							ParticleEffect.CLOUD.display(0.3f, 0f, 0.3f, 0f, 3, loc, 15);
							ParticleEffect.WATER_DROP.display(0.3f, 0f, 0.3f, 0f, 3, loc2, 15);
						}
						if(playercos.get(pls) == "helix"){
							Location loc = new Location(pls.getWorld(), pls.getLocation().getX() + Math.cos(valeure)/2.5, pls.getLocation().getY() + helixheight,  pls.getLocation().getZ() + Math.sin(valeure)/2.5);
							ParticleEffect.REDSTONE.display(0f, 0f, 0f, 0f, 3, loc, 15);
						}
					}
				}
				helixheight += 0.08;
				if(count <= 12){
					feuheight += 0.15;
				}else if(count > 12){
					feuheight -= 0.15;
				}
				if(count >= 24){
					count = 0;
					helixheight = 0;
				}
				count++;
			}
			
		}, 0, 2);
		
	}

}