/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.Runnables;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class Jump {
	
	public static HashMap<Player, Integer> playercheck = new HashMap<Player, Integer>();
	public static HashMap<Player, Integer> playerlives = new HashMap<Player, Integer>();
	
	@SuppressWarnings("deprecation")
	public static void execute(Player p, Block b, Location loc, World w){
		if(loc.getY() > 65){
			if((b != null) && (b.getType().equals(Material.NETHER_WART_BLOCK))){
				if(!playercheck.containsKey(p)){
					playercheck.put(p, 0);
					playerlives.put(p, 3);
					p.sendTitle(ChatColor.GOLD+"Vous commencez le Jump !", ChatColor.RED+"Vie(s) : "+ChatColor.DARK_RED+""+getLives(p));
				}else if(playercheck.get(p) == 9){
					p.sendTitle(ChatColor.GOLD+"Vous avez terminé le Jump !", ChatColor.RED+"Vie(s) : "+ChatColor.DARK_RED+""+getLives(p));
					playercheck.remove(p);
					playerlives.remove(p);
				}
			}else if((b != null) && (b.getType().equals(Material.SEA_LANTERN))){
				if(playercheck.get(p) == 0){
					if(b.getLocation().equals(new Location(w, -1037, 66, 430))){
						playercheck.put(p, 1);
						playerlives.put(p, playerlives.get(p)+1);
						p.sendTitle(ChatColor.GOLD+"Checkpoint 1 !", ChatColor.RED+"Vie(s) : "+ChatColor.DARK_RED+""+getLives(p));
					}
				}
				if(playercheck.get(p) == 2){
					if(b.getLocation().equals(new Location(w, -1037, 66, 428))){
						playercheck.put(p, 2);
						playerlives.put(p, playerlives.get(p)+1);
						p.sendTitle(ChatColor.GOLD+"Checkpoint 2 !", ChatColor.RED+"Vie(s) : "+ChatColor.DARK_RED+""+getLives(p));
					}
				}
			}else if((b != null) && !(b.getType().equals(Material.STAINED_GLASS))){
				if(playercheck.get(p) == 1){
					p.teleport(new Location(w, -1037, 67, 430));
					playerlives.put(p, playerlives.get(p) - 1);
					p.sendTitle(ChatColor.GOLD+"Retour au checkpoint !", ChatColor.RED+"Vie(s) : "+ChatColor.DARK_RED+""+getLives(p));
				}
			}
		}
	}
	
	public static String getLives(Player p){
		int l = playerlives.get(p);
		String c = "Plus de vie";
		if(l == 1){
			c = "♥";
		}else if(l == 2){
			c = "♥♥";
		}else if(l == 3){
			c = "♥♥♥";
		}else if(l == 4){
			c = "♥♥♥♥";
		}else if(l == 5){
			c = "♥♥♥♥♥";
		}else if(l == 6){
			c = "♥♥♥♥♥♥";
		}else if(l == 7){
			c = "♥♥♥♥♥♥♥";
		}else if(l == 8){
			c = "♥♥♥♥♥♥♥♥";
		}else if(l == 9){
			c = "♥♥♥♥♥♥♥♥♥";
		}else if(l == 10){
			c = "♥♥♥♥♥♥♥♥♥♥";
		}else if(l == 11){
			c = "♥♥♥♥♥♥♥♥♥♥♥";
		}else if(l == 12){
			c = "♥♥♥♥♥♥♥♥♥♥♥♥";
		}else if(l == 13){
			c = "♥♥♥♥♥♥♥♥♥♥♥♥♥";
		}else if(l == 14){
			c = "♥♥♥♥♥♥♥♥♥♥♥♥♥♥";
		}else if(l == 15){
			c = "♥♥♥♥♥♥♥♥♥♥♥♥♥♥♥";
		}
		return c;
	}

}
