/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektHub.Runnables;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;

public class stop {
	
	public static void Stop(){
		for(Player pls : Bukkit.getOnlinePlayers()){
			pls.kickPlayer(ChatColor.RED+"Redémarrage du HUB");
		}
		String mapname = Bukkit.getWorlds().get(0).getName();
		Bukkit.getServer().unloadWorld(Bukkit.getWorlds().get(0), true);
		Bukkit.getServer().createWorld(new WorldCreator(mapname));
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "reload");
	}

}
